﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extension
{
    public static class ExtensionMethod
    {
        public static string DateTimeExtension()
        {
            return DateTime.Now.ToString("dd/MM/yyyy");
        }
        public static string DateTimeExtnNull(string dateTime)
        {
            Func<string> getdate = delegate () {
                DateTime d;
                if (DateTime.TryParse(dateTime, out d))
                  return d.ToString("dd/MM/yyyy"); 
                else
                    return "yash";
                
            };
            return dateTime ?? getdate();

        }
        public static string DateTimeExtn(string dateTime)
        {
            DateTime d = Convert.ToDateTime(dateTime);
            return d.ToString("dd/MM/yy hh:mm");
        }
         public static string DateTimeExtn2(string dateTime)
        {
            DateTime d = Convert.ToDateTime(dateTime);
            return d.ToString("dd.MM.yy hh:mm");
        }
        public static string DateTimeExtn3(string dateTime)
        {
            DateTime d = Convert.ToDateTime(dateTime);
            return d.ToString("dd.MM.yy hh:mm:ss");
        }
        public static string DateTimeExtn4(string dateTime)
        {
            DateTime d = Convert.ToDateTime(dateTime);
            return d.ToString("dd/MM/yy hh:mm:ss");
        }
    
     public static string ParsedInt(this string num)
       {
           int a;
          
          int.TryParse(num, out a);
            return num ?? a.ToString();
       }

       public static string ParsedIntDefault(this string num)
       {
           int a;
           bool res = int.TryParse(num, out a);

           return a.ToString(); ;
       }

        public static string ObParsedIntDefault(this object num)
        {
            
             int.TryParse(num.ToString(), out int a);

            return num.ToString() ?? a.ToString(); ;
        }


    }
}

