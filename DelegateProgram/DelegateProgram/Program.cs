﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DelegateProgram.DelegateClass;
using static DelegateProgram.Rectangle;
using Extension;

namespace DelegateProgram
{
    public class DelegateClass
    { // declaring delegate 
        public delegate void AddDel(int a, int b);
        //defining Sum method
        public void Sum(int x, int y)
        {
            Console.WriteLine("sum : "+(x + y));
        }

    }
    class Rectangle
    {
        // declaring delegate 
        public delegate void rectDelegate(double height, double width);

        // "area" method 
        public void Area(double height, double width){
            Console.WriteLine("Area is: {0}", (width * height));
        }

        // "perimeter" method 
        public void Perimeter(double height, double width){
            Console.WriteLine("Perimeter is: {0} ", 2 * (width + height));
        }
    }


        public class Program
    {
        static int Addition(int a, int b)
        {
            return a + b;
        }
        public delegate void Print(int value);

        static void PrintValue(int i)
        {
            Console.WriteLine(i);
        }


        static bool IsUpperCase(string str)
        {
            return str.Equals(str.ToUpper());
        }



        static public void Main(string[] args)
        {
            

        DelegateClass delegateClassObj = new DelegateClass();
            AddDel obj1= new AddDel(delegateClassObj.Sum);
            obj1(100, 60);

            Rectangle rectObj = new Rectangle();
            rectDelegate rectDelegateObj1 = new rectDelegate(rectObj.Area);
            rectDelegateObj1(34.6, 87.5);

            rectDelegate rectDelegateObj2 = new rectDelegate(rectObj.Perimeter);
            rectDelegateObj2(34.6, 87.5);


            rectDelegateObj1 = rectDelegateObj1 + rectDelegateObj2+ rectObj.Perimeter+rectObj.Area;
            rectDelegateObj1.Invoke(34.6, 87.5);

            Func<int, int, int> add = Addition;
            int result = add(10, 10);
            Console.WriteLine(result);

            //Func with Anonymous Method
            Func<int> getRandomNumber = delegate()
            {
                Random rnd = new Random();
                return rnd.Next(1, 1000);
            };

            Console.WriteLine(getRandomNumber());

            //Func with lambda expression
            Func<int> getRandomNo = () => new Random().Next(1, 100);
            Console.WriteLine(getRandomNo());

            //Or 

            Func<int, int, int> sumNo = (x, y) => x + y;
            Console.WriteLine(sumNo(6,7));

           // Anonymous Method
          
            Print print = delegate (int val) {
                Console.WriteLine("Inside Anonymous method. Value: {0}", val);
            };

            print(100);

            Print value = PrintValue;
            value(123);


            //Action delegate
            Action<int> printActionValue = PrintValue;
            printActionValue(123);

            //Anonymous method with Action delegate
            Action<int> printActionDel = delegate (int i)
            {
                Console.WriteLine(i);
            };
            printActionDel(10);


            //Lambda expression with Action delegate

            Action<int> printActionDelLambda = i => Console.WriteLine(i);
            printActionDelLambda(100);

            //Predicate delegate
            Predicate<string> isUpper = IsUpperCase;
            bool predValue = isUpper("Hello");
            Console.WriteLine(predValue);

            //Predicate delegate with anonymous method
            Predicate<string> predicateUpper = delegate (string s) { return s.Equals(s.ToUpper()); };
            bool predicateUpperValue = predicateUpper("HELLO");
            Console.WriteLine(predicateUpperValue);

            //Predicate delegate with lambda expression
            Predicate<string> predicateLambda = s => s.Equals(s.ToUpper());
            bool predicateLAmbdaValue = predicateLambda("hello");
            Console.WriteLine(predicateLAmbdaValue);


            Func<string> getNull = delegate ()
            {
                string nullDateTime = "asddff";
                return ExtensionMethod.DateTimeExtnNull(nullDateTime);
                
            };
            Console.WriteLine(getNull());
            



            Func<string> getNowDate = delegate ()
             {
                 return ExtensionMethod.DateTimeExtension();
             };
            Console.WriteLine(getNowDate());



            Func<string> getDate = delegate ()
            {
                return ExtensionMethod.DateTimeExtn("01/01/2020 22:12");
            };
            Console.WriteLine(getDate());
            Func<string> getDate2 = delegate ()
            {
                return ExtensionMethod.DateTimeExtn2("01/01/2020 22:12");
            };
            Console.WriteLine(getDate2());
            Func<string> getDate3 = delegate ()
            {
                return ExtensionMethod.DateTimeExtn3("01/01/2020 22:12");
            };
            Console.WriteLine(getDate3());
            Func<string> getDate4 = delegate ()
            {
                return ExtensionMethod.DateTimeExtn4("01/01/2020 22:12:00");
            };
            Console.WriteLine(getDate4());

            Func<string> convertedValue = delegate ()
            {
                return ExtensionMethod.ParsedInt("");
            };
            Console.WriteLine(convertedValue());
            Func<string> convertValue = delegate ()
            {
                return ExtensionMethod.ParsedIntDefault("1d");
            };
            Console.WriteLine(convertValue());
            object ob = "12";
            Func<string> objConvertValue = delegate ()
            {
                return ExtensionMethod.ObParsedIntDefault(ob);
            };
            Console.WriteLine(objConvertValue());

            Console.ReadKey();
        }
    }
}
