﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExceptionalHandlingProgram
{
    public class Division
    {
        public void DivideNumber()
        {
            double result;
            try
            {
                Console.Write("Enter the numerator : ");
                int numerator = Convert.ToInt32(Console.ReadLine());
                Console.Write("Enter the denomenator : ");
                int denomenator = Convert.ToInt32(Console.ReadLine());
                if (denomenator == 0)
                    throw new DivideByZeroException();
                result = numerator / denomenator;
               
                Console.WriteLine("division : " + result);

            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("divide by zero.");
            }

            catch (FormatException)
            {
                Console.WriteLine("Format Exception");
            }
            finally
            {
                Console.WriteLine("End of the division");
            }

           
        }
    }
}
            