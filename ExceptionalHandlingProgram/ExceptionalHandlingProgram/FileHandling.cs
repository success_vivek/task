﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ExceptionalHandlingProgram
{
    public class FileHandling
    {
       public void Files()
        {
            string[] filePaths = Directory.GetFiles(@"c:\file\", "*.txt");
            string allFiles=null;
            Console.WriteLine("File already Exist in the directory");
            for(int i=1;i<=filePaths.Length;i++)
            {
                Console.WriteLine(i+". "+filePaths[i-1]);
                allFiles = filePaths[i-1];
            }
           
            Console.WriteLine("********File Handling********");
            Console.Write("Enter the file name you want to create : ");
            string fileName = Console.ReadLine();

            if (allFiles.Contains(fileName)==true)
            {
                Console.WriteLine("file already exist");
            }

            string fileLocation = "C:\\file\\";
            string file = String.Concat(fileLocation, fileName, ".txt");
            Console.WriteLine("Location of File created : " +file);
            StreamWriter file1 = new StreamWriter(file);
            
            Console.WriteLine("Enter the Text that you want to write on File");
            string str = Console.ReadLine();
            file1.WriteLine(str);
            file1.Flush();
            file1.Close();
            StreamReader file2= new StreamReader(file);
            string show = file2.ReadLine();
            Console.Write("Text in the file : ");
            Console.WriteLine(show);
           
            file2.Close();
            
        }
            
    }
}
