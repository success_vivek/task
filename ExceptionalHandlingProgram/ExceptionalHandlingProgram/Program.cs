﻿using System;

namespace ExceptionalHandlingProgram
{
    public class Program
    {
       
        public static void Main(string[] args)
        {
            Console.WriteLine("*******Welcome to the Program*******\n");
            Console.WriteLine("1. Exceptional Handling\n2. File Handling\n");
            Console.Write("Enter your choice(1/2) : ");
            int option = Convert.ToInt32(Console.ReadLine());
            switch (option)
            {
                case 1:
                    Division division = new Division();
                    division.DivideNumber();
                    break;
                case 2:
                    FileHandling file = new FileHandling();
                    file.Files();
                    break;
                default:
                    Console.WriteLine("invalid choice");
                    break;
            }

            Console.ReadKey();
        }
    }
}
