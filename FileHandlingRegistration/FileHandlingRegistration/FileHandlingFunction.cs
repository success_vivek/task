﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandlingRegistration
{
   public class FileHandlingFunction
    {
        public string rName, rMobileNo, rEmail;

        public void AddRecord()
        {
            SqlConnection con = new SqlConnection("Data Source=LAPR143; Initial Catalog=test; integrated security=false ;User ID=sa; Password=Successive");
            con.Open();
           // Console.WriteLine("Registration Form");
            //Console.WriteLine("Enter Id : ");
            //rId = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("\n*****REGISTRATION*****\n");
            Console.Write("Enter Name : ");
            rName = Console.ReadLine();
            Console.Write("Enter the Mobile Number : ");
            rMobileNo = Console.ReadLine();
            Console.Write("Enter the Email Address : ");
            rEmail = Console.ReadLine();

            string storedQuery = "RegistrationProcedure";
            SqlCommand insertValue = new SqlCommand(storedQuery, con);
            insertValue.CommandType = CommandType.StoredProcedure;


            insertValue.Parameters.AddWithValue("name", rName);
            insertValue.Parameters.AddWithValue("mobileNo", rMobileNo);
            insertValue.Parameters.AddWithValue("email", rEmail);
            insertValue.Parameters.Add("@result", SqlDbType.VarChar, 4000);
            insertValue.Parameters["@result"].Direction = ParameterDirection.Output;
            insertValue.ExecuteNonQuery();

            string insertResult = (string)insertValue.Parameters["@result"].Value;
            Console.WriteLine(insertResult);
            
            Console.WriteLine("Record successfully inserted\n");
            con.Close();
        }
        public void DeleteRecord(string name)
        {
            SqlConnection con = new SqlConnection("Data Source=LAPR143; Initial Catalog=test; integrated security=false ;User ID=sa; Password=Successive");
            con.Open();
            SqlCommand deleteRecord = new SqlCommand("delete from Registration where PersonName=@Name", con);
            deleteRecord.Parameters.AddWithValue("Name", name);
            deleteRecord.ExecuteNonQuery();
            con.Close();
        }
        public void UpdateRecord()
        {
            DisplayRecord();
            string newName;
            SqlConnection con = new SqlConnection("Data Source=LAPR143; Initial Catalog=test; integrated security=false ;User ID=sa; Password=Successive");
            con.Open();
        
            Console.WriteLine("Though mobile number");
            string mob = Console.ReadLine();
            SqlCommand updateRecord = new SqlCommand("update Registration set PersonName=@id1 where MobileNo=@id2", con);
            Console.WriteLine("Enter new Name: ");
            newName = Console.ReadLine();
            updateRecord.Parameters.AddWithValue("id1", newName);
            updateRecord.Parameters.AddWithValue("id2", mob);
            updateRecord.ExecuteNonQuery();
            Console.WriteLine("value update");
            DisplayRecord();
            con.Close();
        }
        public void DisplayRecord()
        {
            SqlConnection con = new SqlConnection("Data Source=LAPR143; Initial Catalog=test; integrated security=false ;User ID=sa; Password=Successive");
            con.Open();
            SqlCommand showTable = new SqlCommand("Select * from Registration", con);
            SqlDataReader sdr = showTable.ExecuteReader();
            Console.WriteLine("\nData in the Table\n");
            while (sdr.Read())
            {
                Console.WriteLine(sdr["Personid"] + " " + sdr["PersonName"] + " " + sdr["MobileNo"] + " " + sdr["Email"]); // Displaying Record  
            }
            Console.WriteLine();
            con.Close();
        }
    }
}
