﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandlingRegistration
{
    class Program
    {
        static void Main(string[] args)
        {


            SqlConnection con = new SqlConnection("Data Source=LAPR143; Initial Catalog=test; integrated security=false ;User ID=sa; Password=Successive");
            con.Open();
            //SqlCommand createTable = new SqlCommand("create table Registration (Personid int IDENTITY(1,1) PRIMARY KEY, PersonName varchar(50), MobileNo varchar(13) NOT NULL,Email varchar(50) NOT NULL)", con);
            //createTable.ExecuteNonQuery();
            //Console.WriteLine("Table created Successfully");
            FileHandlingFunction fileHandlingFunctionObj = new FileHandlingFunction();
            int choice;
            do
            {
                Console.WriteLine("1. Enter the Record\n2. Update a record\n3. Delete a Record\n4. Display Database");
                Console.Write("Enter your choice: ");
                choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                        fileHandlingFunctionObj.DisplayRecord();
                        fileHandlingFunctionObj.AddRecord();
                        break;
                    case 2:
                        fileHandlingFunctionObj.UpdateRecord();
                        break;
                    case 3:
                        string name;
                        fileHandlingFunctionObj.DisplayRecord();
                        Console.Write("\nWrite the name of the person you want delete the record : ");
                        name = Console.ReadLine();
                        fileHandlingFunctionObj.DeleteRecord(name);
                        Console.WriteLine("record deleted successfully");
                        fileHandlingFunctionObj.DisplayRecord();
                        break;
                    case 4:
                        fileHandlingFunctionObj.DisplayRecord();
                        break;
                    default:
                        Console.WriteLine("Wrong Input");
                        break;
                }
            } while (choice < 5);
            Console.ReadKey();
            con.Close();
        }
    }
}
