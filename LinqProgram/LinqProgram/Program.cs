﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> nameList = new List<string>();
            char cont;

            do
            {
                Console.WriteLine("1. Store names of duplicate type.\n 2. Enter lists to perfrom operations");
                int choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        do
                        {
                            Console.Write("Enter a name: ");
                            string name = Console.ReadLine();

                            //var result = nameList.Where(s => s.Contains(name));
                            var result = from x in nameList where x.Contains(name) select x;
                            int count = result.Count();
                            Console.WriteLine("Same records are : " + count);
                            if (count > 0)
                            {

                                nameList.Add(name + "_" + count);
                            }
                            else
                            {
                                nameList.Add(name);
                            }
                            var fetch = from fetchName in nameList select new { fetchName };
                            Console.WriteLine("\n***Records***");
                            foreach (var fetchData in fetch)
                            {
                                Console.WriteLine("{0}", fetchData.fetchName);
                            }
                            //                var totalRecords = (from x in fetch select x).Count();

                            var totalRecords = fetch.Count();
                            Console.WriteLine("Total records= " + totalRecords);
                            Console.WriteLine("Do you want to enter more records! (Y/N)");
                            cont = Convert.ToChar(Console.ReadLine());

                        } while (cont == 'y' || cont == 'Y');
                        break;
                    case 2:
                        List<int> str1 = new List<int>();
                        Console.WriteLine("Enter the 5 integers in the list 1");
                        str1.Add(Convert.ToInt32(Console.ReadLine()));
                        str1.Add(Convert.ToInt32(Console.ReadLine()));
                        str1.Add(Convert.ToInt32(Console.ReadLine()));
                        str1.Add(Convert.ToInt32(Console.ReadLine()));
                        str1.Add(Convert.ToInt32(Console.ReadLine()));
                        Console.WriteLine("Elemnets Entered by you are :");
                        foreach (int strgList1 in str1)
                        {
                            Console.WriteLine(strgList1);
                        }

                        List<string> str2 = new List<string>();
                        Console.WriteLine("Enter the 5 elements in the list 2");
                        str2.Add(Console.ReadLine());
                        str2.Add(Console.ReadLine());
                        str2.Add(Console.ReadLine());
                        str2.Add(Console.ReadLine());
                        str2.Add(Console.ReadLine());
                        Console.WriteLine("Elements Entered by you are :");
                        foreach (string strgList2 in str2)
                        {
                            Console.WriteLine(strgList2);
                        }

                        var useWhere = str1.Where(s => s > 10);
                        Console.WriteLine("Numbers greater than 10 in list 1 are : ");
                        foreach (int str in useWhere)
                        {
                            Console.WriteLine(str);
                        }

                        var strAscOrder = str1.OrderBy(s => s);
                        Console.WriteLine("Numbers in ascending order in list 1 are : ");
                        foreach (int str in strAscOrder)
                        {
                            Console.WriteLine(str);
                        }

                        var groupedResult = str2.GroupBy(s => s);
                        Console.WriteLine("Group data in list 2 are : ");
                        foreach (var str in groupedResult)
                        {
                            Console.WriteLine(str.Key);
                        }

                        var lookupResult = str2.ToLookup(s => s);
                        Console.WriteLine("Group data(lookup) in list 2 are : ");
                        foreach (var str in lookupResult)
                        {
                            Console.WriteLine(str.Key);
                        }

                        
                        Console.WriteLine("1st Element in List 1: {0}", str1.ElementAt(0));
                        Console.WriteLine("1st Element in List 2: {0}", str2.ElementAt(0));

                        Console.WriteLine("2nd Element in List 1: {0}", str1.ElementAt(1));
                        Console.WriteLine("2nd Element in List 2: {0}", str2.ElementAt(1));

                        Console.WriteLine("3rd Element in List 1: {0}", str1.ElementAtOrDefault(2));
                        Console.WriteLine("3rd Element in List 1: {0}", str2.ElementAtOrDefault(2));

                        Console.WriteLine("1st Element in List 1: {0}", str1.FirstOrDefault());
                        Console.WriteLine("1st Even Element in List 1: {0}", str1.FirstOrDefault(i => i % 2 == 0));

                        Console.WriteLine("1st Element in List 2: {0}", str2.FirstOrDefault());

                        Console.WriteLine("Last Element in List 1: {0}", str1.Last());

                        Console.WriteLine("Last Even Element in List 1: {0}", str1.Last(i => i % 2 == 0));

                        Console.WriteLine("Last Element in List 2: {0}", str2.Last());


                        Console.WriteLine("The only element which is less than 2 in List 1: {0}", str1.SingleOrDefault(i => i < 2));

                        List<string> strList2 = new List<string>() { "three", "Two" };
                        var unionString = str2.Union(strList2);
                        Console.WriteLine("Union of Strings");
                        foreach (string str in unionString)
                        {
                            Console.WriteLine(str);
                        }

                        var innerJoinResult = str2.Join(strList2, List1 => List1, List2 => List2, (List1, List2) => List1);
                        Console.WriteLine("Inner Join");
                        foreach(var result in innerJoinResult)
                        {
                            Console.WriteLine(result);
                        }

                        bool areAllResult = str1.All(s => s > 12 && s < 20);
                        Console.WriteLine("All result : list 1>12 and list1 < 20 : "+areAllResult);

                        bool areAnyResult = str1.Any(s => s > 12 && s < 20);
                        Console.WriteLine("Any result : list 1>12 and list1 < 20 : "+areAnyResult);



                        var resultTakeWhile = str2.TakeWhile((s, i) => s.Length > i);
                        Console.WriteLine("Take While Result: ");
                        foreach(var resultTake in resultTakeWhile)
                        {
                            Console.WriteLine(resultTake);
                        }





                        break;
                        

                    default:
                        Console.WriteLine("Enter wrong input!");
                        break;
                }
             
                Console.WriteLine("Do you want to continue?  y/n : ");
                cont = Convert.ToChar(Console.ReadLine());
            } while (cont == 'y' || cont == 'Y');

            Console.ReadLine();

        }

    }
    }
