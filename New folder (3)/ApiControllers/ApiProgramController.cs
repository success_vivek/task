﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ApiProgramDemo.ApiControllers
{
    public class ApiProgramController : ApiController
    {

        [HttpGet]
        public int GetData()
        {
            return 10;
        }
        
        public int GetData2(int a, int b) //Get data from query string
        {
            return a + b;
        }

    }

}
