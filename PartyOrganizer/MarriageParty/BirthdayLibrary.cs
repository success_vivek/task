﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarriageParty
{
    
    public class BirthdayLibrary : MainLibrary
    {
        public override void VenueDate()
        {
            Console.Write("\nBooking Date : ");
            DateTime vDate = DateTime.Now;              // Get current DateTime. It can be any DateTime object in your code.  
            Console.WriteLine(vDate.ToString("dd/MM/yyyy"));
            base.PartyDate = vDate;

            Console.Write("\nEnter the venue : ");
            string birthdayVenue = Console.ReadLine();
            Venue = birthdayVenue;
        }

        public override void GuestInfo(int noOfGuest)
        {
            base.Guests = noOfGuest;
        }

        public void BirthdayCake()
        {
            double cakeSize;
            double cakePrice;
            Console.Write("\nDo you want cake? Y/N :  ");
            string wantCake = Console.ReadLine();
            if (wantCake == "y" || wantCake == "Y")
            {
                while (wantCake == "y" || wantCake == "Y")
                {
                    Console.Write("\nEnter the size of cake in Kg :");
                    if (double.TryParse(Console.ReadLine(), out cakeSize))
                    {
                        cakePrice = cakeSize * 100;
                        base.ExtraPrice = cakePrice;
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Enter the Kg in Numbers");
                        Console.Write("\nDo you want cake? Y/N :  ");
                        wantCake = Console.ReadLine();
                    }
                }
            }
        }
        public override void Charges()
        {
            base.Catering = 30;
            base.Decoration = 20;
            base.PartyType = "Birthday";
            BirthdayCake();
            base.Package();
            base.DisplayReciept();
            Console.WriteLine("\n\n*Note: Extra Charges includes Cake and Decoration price includes balloons, decoration, servents and other things according to the no. of the guests. \n");
        }
    }
}
