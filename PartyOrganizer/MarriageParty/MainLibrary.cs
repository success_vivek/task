﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MarriageParty
{
    public abstract class MainLibrary
    {
        public string PartyType { get; set; }
        public string Venue { get; set; }
        public double PartyPackage { get; set; }
        public int Guests { get; set; }
        public double Catering { get; set; }
        public double Decoration { get; set; }
        public abstract void GuestInfo(int noOfGuest);
        public DateTime PartyDate;
        public double ExtraPrice;
        public void Package()
        {
            string tryAgain = "Y";

            Console.WriteLine("\n****** SELECT THE PACKAGE ******");
            Console.WriteLine("1. Base Package\n2. Standard Package\n3. Premium Package");
            Console.Write("Enter your Choice  (1, 2, 3) :  ");
            int choice, count = 0;
            do
            {
                if (count > 0)
                {
                    break;
                }

                if (int.TryParse(Console.ReadLine(), out choice))
                {
                    count++;
                    switch (choice)
                    {
                        case 1:
                            PartyPackage = (Catering + Decoration) * Guests + ExtraPrice;
                            Console.WriteLine("estimate price : " + PartyPackage);
                            break;
                        case 2:
                            Catering = Catering * 1.5;
                            Decoration *= 1.5;
                            PartyPackage = (Catering + Decoration) * Guests + ExtraPrice;
                            Console.WriteLine("estimate price : " + PartyPackage);
                            break;
                        case 3:
                            Catering *= 2;
                            Decoration *= 2;
                            PartyPackage = (Catering + Decoration) * Guests + ExtraPrice;
                            Console.WriteLine("estimate price : " + PartyPackage);
                            break;
                        default:
                            Console.WriteLine("Invalid Choice");
                            break;
                    }
                }
                else
                {

                    Console.WriteLine("Enter valid input");
                    Console.Write("Try again? Y/N  :  ");
                    tryAgain = Console.ReadLine();
                    Console.WriteLine("\n****** SELECT THE PACKAGE ******");
                    Console.WriteLine("1. Base Package\n2. Standard Package\n3. Premium Package");
                    Console.Write("Enter your Choice  (1, 2, 3) :  ");

                }

            } while (tryAgain == "y" || tryAgain == "Y");
        }
        public abstract void VenueDate();
        public abstract void Charges();
        public void DisplayReciept()
        {
            string fileLocation = "C:\\Users\\viveks\\Desktop\\Booking files\\";
            

            if (!Directory.Exists(fileLocation))
            {
                Directory.CreateDirectory(fileLocation);
            }
            string allFiles = "file";
            string[] filePaths = Directory.GetFiles(@"C:\Users\viveks\Desktop\Booking files\", "*.txt");
            Console.WriteLine("File already Exist in the directory");
            for (int i = 1; i <= filePaths.Length; i++)
            {
                Console.WriteLine(i + ". " + filePaths[i - 1]);
                allFiles = filePaths[i - 1];
            }

            Console.WriteLine("\n\n********Reciept File********\n");

            Console.Write("Enter the customer name to make a file : ");
            string fileName = Console.ReadLine();
            start:
            string file = String.Concat(fileLocation, fileName, ".txt");


            if (allFiles.Contains(file))
            {
                Console.WriteLine("Alert! file already exist");
                Console.WriteLine("Do you want to continue? Y/N");
                string makeFile = Console.ReadLine();
                if (makeFile == "y" || makeFile == "Y")
                {
                    Console.WriteLine("Do you want to append this file? Y/N");
                    string appendFile = Console.ReadLine();
                    if (appendFile == "y" || appendFile == "Y")
                    {

                        Console.WriteLine("Successfully created, Location of File : " + file);

                        using (StreamWriter booking = File.AppendText(file))
                        {
                            booking.WriteLine("\n\n***** Reciept ******\n");
                            booking.WriteLine("Venue                    : " + Venue);
                            booking.WriteLine("Date                     : " + PartyDate);
                            booking.WriteLine("Party type               : " + PartyType);
                            booking.WriteLine("No. of guests            : " + Guests);
                            booking.WriteLine("Decoration cost          : Rs. " + Decoration * Guests);
                            booking.WriteLine("* Extra Charges          : Rs. " + ExtraPrice);
                            booking.WriteLine("Catering / food charges  : Rs. " + Catering * Guests);
                            booking.WriteLine("Cost per person          : Rs. " + (PartyPackage / Guests));
                            booking.WriteLine("                             ---------");
                            booking.WriteLine("Total Cost               : Rs. " + PartyPackage);
                            booking.WriteLine("                             ---------");
                            booking.WriteLine("           *********THANK YOU*********");
                            booking.Close();

                        }
                    }
                    else
                    {
                        //  string fileLocation = "C:\\Users\\viveks\\Desktop\\Booking files\\";
                        //string file = String.Concat(fileLocation, fileName, ".txt");
                        Console.WriteLine("Successfully created, Location of File : " + file);

                        using (StreamWriter booking = File.CreateText(file))
                        {
                            booking.WriteLine("\n\n***** Reciept ******\n");
                            booking.WriteLine("Venue                    : " + Venue);
                            booking.WriteLine("Date                     : " + PartyDate);
                            booking.WriteLine("Party type               : " + PartyType);
                            booking.WriteLine("No. of guests            : " + Guests);
                            booking.WriteLine("Decoration cost          : Rs. " + Decoration * Guests);
                            booking.WriteLine("* Extra Charges          : Rs. " + ExtraPrice);
                            booking.WriteLine("Catering / food charges  : Rs. " + Catering * Guests);
                            booking.WriteLine("Cost per person          : Rs. " + (PartyPackage / Guests));
                            booking.WriteLine("                             ---------");
                            booking.WriteLine("Total Cost               : Rs. " + PartyPackage);
                            booking.WriteLine("                             ---------");
                            booking.WriteLine("           *********THANK YOU*********");
                            booking.Close();

                        }
                    }
                }
                else
                {
                    Console.WriteLine("Enter the file name again : ");
                    fileName = Console.ReadLine();
                    goto start;

                }
            }
            else
            {
                //string fileLocation = "C:\\Users\\viveks\\Desktop\\Booking files\\";
                //string file = String.Concat(fileLocation, fileName, ".txt");
                Console.WriteLine("Successfully created, Location of File : " + file);

                using (StreamWriter booking = File.CreateText(file))
                {
                    booking.WriteLine("\n\n***** Reciept ******\n");
                    booking.WriteLine("Venue                    : " + Venue);
                    booking.WriteLine("Date                     : " + PartyDate);
                    booking.WriteLine("Party type               : " + PartyType);
                    booking.WriteLine("No. of guests            : " + Guests);
                    booking.WriteLine("Decoration cost          : Rs. " + Decoration * Guests);
                    booking.WriteLine("* Extra Charges          : Rs. " + ExtraPrice);
                    booking.WriteLine("Catering / food charges  : Rs. " + Catering * Guests);
                    booking.WriteLine("Catering / food charges  : Rs. " + Catering * Guests);
                    booking.WriteLine("Cost per person          : Rs. " + (PartyPackage / Guests));
                    booking.WriteLine("                             ---------");
                    booking.WriteLine("Total Cost               : Rs. " + PartyPackage);
                    booking.WriteLine("                             ---------");
                    booking.WriteLine("           *********THANK YOU*********");
                    booking.Close();

                }
            }
            



            using (StreamReader bookingFile = File.OpenText(file))
            {
                string s;
                while ((s = bookingFile.ReadLine()) != null)
                {
                    Console.WriteLine(s);
                }
            }
            /*
                StreamReader file2 = new StreamReader(file);
                string show = file2.ReadLine();
                Console.Write("Data saved in the " + fileName + ".txt");
                Console.WriteLine(show);
                */
        }
    }
}
