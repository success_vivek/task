﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarriageParty
{ 
    public class MarriageLibrary : MainLibrary
    {
        public override void VenueDate()
        {
            Console.Write("\nEnter the venue : ");
            string anniversaryVenue = Console.ReadLine();
            Venue = anniversaryVenue;
            Console.Write("\nBooking Date : ");
            // Get current DateTime. It can be any DateTime object in your code.  
            DateTime vDate = DateTime.Now;
            Console.Write(vDate.ToString("dd/MM/yyyy"));
            base.PartyDate = vDate;
        }

        public override void GuestInfo(int noOfGuest)
        {
            base.Guests = noOfGuest;
        }

        public void VarmalaTable()
        {

            double varmalaPrice;
            Console.Write("\n\nDo you want Round table Varmala? Y/N  :  ");
            string wantCake = Console.ReadLine();
            if (wantCake == "y" || wantCake == "Y")
            {
                Console.WriteLine("Congratulations Varmala Booked\n");
                varmalaPrice = 2000;
                base.ExtraPrice = varmalaPrice;
            }
            else
            {
                Console.WriteLine("Varmala round table not booked");
            }
        }
        public override void Charges()
        {
            base.Catering = 60;
            base.Decoration = 40;
            base.PartyType = "Marriage";
            VarmalaTable();
            base.Package();
            base.DisplayReciept();
            Console.WriteLine("\n\n*Note: Extra Charges includes Round table Varmala and Decoration price includes balloons, decoration, servents and other things according to the no. of the guests. \n");
        }


    }
}
