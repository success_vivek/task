﻿using System;
using MarriageParty;
using System.IO;
namespace PartyOrganizer
{
   public abstract class Party
    {
        public string PartyType { get; set; }
        public string Venue { get; set; }
        public double PartyPackage { get; set; }
        public int Guests { get; set; }
        public double Catering { get; set; }
        public double Decoration { get; set; }
        public abstract void GuestInfo(int noOfGuest);
        public DateTime PartyDate;
        public double ExtraPrice;
        public abstract void VenueDate();
        public abstract void Charges();
        public abstract void DisplayReciept();
        public abstract void Package();
        
        
        /*
        {
            string tryAgain="Y";

            Console.WriteLine("\n****** SELECT THE PACKAGE ******");
            Console.WriteLine("1. Base Package\n2. Standard Package\n3. Premium Package");
            Console.Write("Enter your Choice  (1, 2, 3) :  ");
            int choice, count=0;
            do
            {
                if (count > 0)
                {
                    break;
                }

                if (int.TryParse(Console.ReadLine(), out choice))
                {
                    count++;
                    switch (choice)
                    {
                        case 1:
                            PartyPackage = (Catering + Decoration) * Guests + ExtraPrice;
                            Console.WriteLine("estimate price : " + PartyPackage);
                            break;
                        case 2:
                            Catering = Catering * 1.5;
                            Decoration *= 1.5;
                            PartyPackage = (Catering + Decoration) * Guests + ExtraPrice;
                            Console.WriteLine("estimate price : " + PartyPackage);
                            break;
                        case 3:
                            Catering *= 2;
                            Decoration *= 2;
                            PartyPackage = (Catering + Decoration) * Guests + ExtraPrice;
                            Console.WriteLine("estimate price : " + PartyPackage);
                            break;
                        default:
                            Console.WriteLine("Invalid Choice");
                            break;
                    }
                }
                else
                {

                    Console.WriteLine("Enter valid input");
                    Console.Write("Try again? Y/N  :  ");
                    tryAgain = Console.ReadLine();
                    Console.WriteLine("\n****** SELECT THE PACKAGE ******");
                    Console.WriteLine("1. Base Package\n2. Standard Package\n3. Premium Package");
                    Console.Write("Enter your Choice  (1, 2, 3) :  ");
                    
                }

            } while (tryAgain == "y" || tryAgain == "Y");
        }
        
        public void DisplayReciept()
        {
            string[] filePaths = Directory.GetFiles(@"C:\Users\viveks\Desktop\Booking files\", "*.txt");
            Console.WriteLine("File already Exist in the directory");
            for (int i = 1; i <= filePaths.Length; i++)
            {
                Console.WriteLine(i + ". " + filePaths[i - 1]);
            }

            Console.WriteLine("********File Handling********");
            Console.Write("Enter the customer name to make a file : ");
            string fileName = Console.ReadLine();
            string fileLocation = "C:\\Users\\viveks\\Desktop\\Booking files\\";
            string file = String.Concat(fileLocation, fileName, ".txt");
            Console.WriteLine("Location of File created : " + file);

            using (StreamWriter booking = File.AppendText(file))
            {
                booking.WriteLine("\n\n***** Reciept ******\n");
                booking.WriteLine("Venue                    : " + Venue);
                booking.WriteLine("Date                     : " + PartyDate);
                booking.WriteLine("Party type               : " + PartyType);
                booking.WriteLine("No. of guests            : " + Guests);
                booking.WriteLine("Decoration cost          : Rs. " + Decoration * Guests);
                booking.WriteLine("* Extra Charges          : Rs. " + ExtraPrice);
                booking.WriteLine("Catering / food charges  : Rs. " + Catering * Guests);
                booking.WriteLine("Cost per person          : Rs. " + (PartyPackage / Guests));
                booking.WriteLine("                             ---------");
                booking.WriteLine("Total Cost               : Rs. " + PartyPackage);
                booking.WriteLine("                             ---------");
                booking.WriteLine("           *********THANK YOU*********");
            }
        }
    */
        }
    
    class Program
    {
        static void Main(string[] args)
        {
            string condition;
            do
            {
                
                BirthdayLibrary BirthdayBooking = new BirthdayLibrary();
               
                AnniversaryLibrary AnniversaryBooking = new AnniversaryLibrary();
                
                MarriageLibrary MarriageBooking = new MarriageLibrary();
                Console.WriteLine("*** WELCOME TO PARTY ORGANIZER ***");
                Console.WriteLine("Select the type of party you want");
                Console.WriteLine("1. Birthday Party\n2. Anniversary Party\n3. Marriage-related Party");
                int partyType;
                Console.Write("Enter your Choice : ");
                
                if (int.TryParse(Console.ReadLine(), out partyType))
                {

                    if (partyType == 1 || partyType == 2 || partyType == 3)
                    {
                        Console.Write("\nEnter the no. of Guests gether together : ");
                        int partyGuests;
                            if (int.TryParse(Console.ReadLine(), out partyGuests))
                            {
                                switch (partyType)
                                {
                                    case 1:
                                        BirthdayBooking.VenueDate();
                                        BirthdayBooking.GuestInfo(partyGuests);
                                        BirthdayBooking.Charges();
                                        break;
                                    case 2:
                                        AnniversaryBooking.VenueDate();
                                        AnniversaryBooking.GuestInfo(partyGuests);
                                        AnniversaryBooking.Charges();
                                        break;
                                    case 3:
                                        MarriageBooking.VenueDate();
                                        MarriageBooking.GuestInfo(partyGuests);
                                        MarriageBooking.Charges();
                                        break;
                                    default:
                                        Console.WriteLine("Invalid choice ! Please enter a valid Choice ");
                                        break;
                                }
                            }
                            else
                            {
                                Console.WriteLine("\nAlert! Enter the Valid input in numbers.\n\n");
                            }                        
                    }
                    else
                    {
                        Console.WriteLine("\nError! Please Select the Correct Booking Type.\n\n");
                    }
                }
                else
                {
                    Console.WriteLine("\nAlert! Enter the correct option.\n\n");
                }
                Console.Write("Do you want to make booking again? Y/N  :  ");
                condition = Console.ReadLine();
                Console.WriteLine("\n");
            } while (condition == "y" || condition == "Y");
        }
    }
}