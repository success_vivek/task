﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Data;
using System.Windows;

namespace SQLConnection
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
              List<Person> personList = new List<Person>();

                int cid;
                string cname;
                string cDOB;
                

                SqlConnection con = new SqlConnection("Data Source=LAPR143; Initial Catalog=test; integrated security=false ;User ID=sa; Password=Successive");

                    
                    SqlDataAdapter sqlAdapter = new SqlDataAdapter("select * from databasetest;", con);
                    DataSet ds = new DataSet();
                    SqlCommandBuilder scBuilder = new SqlCommandBuilder(sqlAdapter);
                    sqlAdapter.Fill(ds);
                    ds.Tables[0].Rows[1][1] = "VIVEK";
                    sqlAdapter.Update(ds.Tables[0]);
                    Console.WriteLine("success");
                    Console.ReadKey();

                

                

                  con.Open();
                  // SqlCommand createTable = new SqlCommand("create table databasetest (PersonId int not null, PersonName varchar(100), DateOfBirth date)", con);
                  // createTable.ExecuteNonQuery();
                  Console.WriteLine("Table created Successfully");
                  Console.ReadKey();


                  Console.WriteLine("Data insertion");
                  Console.WriteLine("Enter Id : ");
                  cid = Convert.ToInt32(Console.ReadLine());
                  Console.WriteLine("Enter Name : ");   
                  cname = Console.ReadLine();
                  Console.WriteLine("enter the DOB : (MM/DD/YYYY)  ");
                  cDOB = Console.ReadLine();


                  SqlCommand insertValue = new SqlCommand("insert into databasetest (PersonId, PersonName, DateOfBirth )values(@id ,@name, @dob)", con);
                  insertValue.Parameters.AddWithValue("id", cid);
                  insertValue.Parameters.AddWithValue("name", cname);
                  insertValue.Parameters.AddWithValue("Dob", cDOB);
                  insertValue.ExecuteNonQuery();
                  Console.WriteLine("Value inserted");


                  SqlCommand updateRecord = new SqlCommand("update databasetest set PersonId=@id1 where PersonId=@id2", con);
                  updateRecord.Parameters.AddWithValue("id1", 3);
                  updateRecord.Parameters.AddWithValue("id2", 1);
                  updateRecord.ExecuteNonQuery();
                  Console.WriteLine("value update");


                  SqlCommand deleteRecord = new SqlCommand("delete from databasetest where PersonId=@id", con);
                  deleteRecord.Parameters.AddWithValue("id", 3);
                  deleteRecord.ExecuteNonQuery();
                  Console.WriteLine("record deleted");


                  SqlCommand showTable = new SqlCommand("Select * from databasetest", con);
                  SqlDataReader sdr = showTable.ExecuteReader();

                  DataTable DataTab = new DataTable();
                  DataTab.Load(sdr);
                  foreach (DataRow dr in DataTab.Rows)
                  {
                      personList.Add(new Person
                      {
                          ID = Convert.ToInt32(dr["PersonId"]),
                          Name = dr["PersonName"].ToString(),
                          Dob = dr["DateOfBirth"].ToString()
                      });
                  }
                  foreach (var i in personList)
                  {
                      Console.WriteLine(i.ID + " " + i.Name + " " + i.Dob);
                  }


                  SqlCommand countTable = new SqlCommand("select count(*) from databasetest", con);
                  int count = 0;
                  count = (int)countTable.ExecuteScalar();
                  Console.WriteLine("Total no. of records= " + count);
                  /*   // Iterating Data  
                     while (sdr.Read())
                     {
                         Console.WriteLine(sdr["PersonId"] + " " + sdr["PersonName"] + " " + sdr["DateOfBirth"]); // Displaying Record  
                     } 
                     */
                  SqlCommand indexkey = new SqlCommand("SP_Primary", con);
                  indexkey.CommandType = CommandType.StoredProcedure;
                  indexkey.Parameters.AddWithValue("key", 2);

                  indexkey.Parameters.Add("@result", SqlDbType.Int);
                  indexkey.Parameters["@result"].Direction=ParameterDirection.Output;
                  indexkey.Parameters.Add("@tab", SqlDbType.VarChar,4000);
                  indexkey.Parameters["@tab"].Direction = ParameterDirection.Output;
                  indexkey.ExecuteNonQuery();
                  int cx = (int)indexkey.Parameters["@result"].Value;
                  Console.WriteLine("total no. of record with same id are : "+cx);

                                  SqlDataReader sdrr = indexkey.ExecuteReader();
                                  DataTable dtt = new DataTable();
                                  dtt.Load(sdrr);
                                  foreach(DataRow dx in dtt.Rows)
                                  {
                                      personList.Add(new Person
                                      {
                                          ID = Convert.ToInt32(dx["PersonId"]),
                                          Name = dx["PersonName"].ToString(),
                                          Dob = dx["DateOfBirth"].ToString()
                                      });
                                  }
                                  foreach (var i in personList)
                                  {
                                      Console.WriteLine(i.ID + " " + i.Name + " " + i.Dob);
                                  }
                  con.Close();
                  Console.ReadKey();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.ReadKey();
            }
            finally { } 
        }
    }
}
