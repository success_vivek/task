USE Course


INSERT INTO SUBJECT VALUES
(1,'HINDI',101,40),
(2,'ENGLISH',102,30),
(3,'COMPUTER SCIENCE',103,50),
(4,'MATHS',104,70),
(5,'SST',105,90),
(6,'SCIENCE',107,80);

SELECT* FROM SUBJECT;

ALTER TABLE SUBJECT
ADD CONSTRAINT MIN_MARKS DEFAULT 00 FOR SUB_MARKS;

SELECT  a.STUD_ID, a.STUD_FEES, b.STUD_NAME
   FROM STUDENT a, STUDENT b
    WHERE a.STUD_FEES > b.STUD_FEES; 

SELECT Count(STUD_ID), COUNT(STUD_AGE),STUD_NAME
   FROM STUDENT
   GROUP BY STUD_NAME
   HAVING COUNT(STUD_AGE) >= 1;


SELECT * FROM STUDENT WHERE STUD_FEES > 28000 ORDER BY STUD_ADDRESS DESC;

SELECT DISTINCT STUD_NAME ,STUD_FEES,STUD_ADDRESS FROM STUDENT WHERE STUD_FEES LIKE '2%0';



SELECT * FROM SUBJECT WHERE SUB_NAME='MATHS' OR SUB_NAME='COMPUTER SCIENCE';

use Course
SELECT *
FROM SUBJECT
SELECT *
FROM STUDENT
SELECT STUD_NAME,STUD_ID
FROM STUDENT
WHERE STUD_ID = ANY (SELECT STUDENT_ID FROM SUBJECT WHERE SUB_NAME = 'HINDI')
SELECT *
FROM STUDENT, SUBJECT
WHERE STUD_ID =STUDENT_ID;


use Course
SELECT Count(STUD_ID), COUNT(STUD_AGE),STUD_NAME, STUD_ADDRESS
   FROM STUDENT
   GROUP BY STUD_NAME,STUD_ADDRESS
   HAVING COUNT(STUD_AGE) >= 1;

SELECT DISTINCT STUD_ADDRESS, STUD_FEES
FROM STUDENT;

use Course
declare @FEES INT= 5400
SELECT STUD_ADDRESS, STUD_FEES
FROM STUDENT
IF @FEES > 25000
    BEGIN
        PRINT 'FEES IS GREATER';
    END
    ELSE
    BEGIN
        PRINT 'FEES IS LESSER';
    END;

SELECT LEN('This is string') AS Length

SELECT LEFT ('SQL Server 2008', 3) As SQL

SELECT RIGHT ('SQL Server 2008', 4) As SQL

DECLARE @TEXT VARCHAR(20)='     HELLO HOW ARE YOU?'
SELECT LTRIM (@TEXT) As SQL

SELECT LTRIM ('     HELLO HOW ARE YOU?') As SQL

SELECT SUBSTRING ('HELLO HOW ARE YOU?',1,5) As SQL

SELECT REPLACE ('HELLO HOW ARE YOU?','HELLO','HEY') As SQL

SELECT STUFF('HELLO HOW ARE YOU?',1,5,'HEY') AS SQL

SELECT ASCII('E');

SELECT CHARINDEX('HOW','HELLO HOW ARE YOU')

SELECT DIFFERENCE(7,3);

SELECT MAX(STUD_FEES)
FROM STUDENT;

SELECT SUM(STUD_FEES)
FROM STUDENT;

SELECT CONCAT('HELLO', 'HIIII');

SELECT 'HELLO ' + 'HIII';

Select getdate() as date_time

Select datepart(MONTH, getdate()) as Date_Time

Select dateadd(day, 10, getdate()) as afer_10days_datetime

Select datediff(day, 2020-02-19, 2020-02-11) as differnce

SELECT CONVERT(VARCHAR(11),GETDATE(),100)

Select abs(-34);

Select sign(-7);

Select floor(5.7);

Select power(2,5);

Select round(5.7,6.8);

Select sqrt(9);

Select asin(1);

Select sin(1);

SELECT RAND();

select CEILING(67.456);

select ROUND(544.55,0);

select pi();

use Course;
select *
from STUDENT;





BEGIN
    DECLARE @Total_Fees INT;
 
    SELECT 
        @Total_Fees = SUM(STUD_FEES)
    FROM
        STUDENT
 
 
    IF @Total_Fees > 100
    BEGIN
        PRINT 'Great! The Total FEES is greater than 10000';
    END
	ELSE
    BEGIN
        PRINT 'Total FEES is less than the 10000';
    END
END

BEGIN
    DECLARE @x INT = 10,
            @y INT = 20;
 
    IF (@x > 0)
    BEGIN
        IF (@x < @y)
            PRINT 'x > 0 and x < y';
        ELSE
            PRINT 'x > 0 and x >= y';
    END 
END

USE Course
SELECT
    STUD_ID,
    STUD_NAME,
    STUD_AGE
FROM
    STUDENT s
WHERE
    EXISTS (
        SELECT
            STUDENT_ID
        FROM
            SUBJECT
        WHERE
            STUDENT_ID = STUD_ID
    )

ORDER BY
    STUD_ID;
    

USE Course
SELECT
    STUD_ID,
    STUD_NAME,
    STUD_AGE
FROM
    STUDENT;

select *
FROM SUBJECT;  


--Q1 CALCULATOR
declare @VAL1 DECIMAL
DECLARE @VAL2 DECIMAL
DECLARE @DIV DECIMAL(20,4)
DECLARE @STR VARCHAR(20)
SET @VAL1=40
SET @VAL2=0


	BEGIN
	IF (@VAL2 = 0)
	BEGIN
		SET @STR='VALUE_2 IS ZERO 0'
			SELECT @VAL1 AS VALUE1, @VAL2 AS VALUE2, (@VAL1+@VAL2) AS ADDITION, (@VAL1-@VAL2) AS SUBTRACTION, (@VAL1*@VAL2) AS MULTIPLY, @STR AS DIVISION;
          END
    ELSE
	BEGIN
           SET @DIV=@VAL1/@VAL2;
		   SELECT @VAL1 AS VALUE1, @VAL2 AS VALUE2, (@VAL1+@VAL2) AS ADDITION, (@VAL1-@VAL2) AS SUBTRACTION, (@VAL1*@VAL2) AS MULTIPLY, @DIV AS DIVISION;
    END 
	END
	

--Q2 RECURSIVE SUBTRACTION
DECLARE @V1 INT
DECLARE @V2 INT
DECLARE @RESULT INT
SET @V1=40
SET @V2=5
IF(@V1>0 AND @V2>0 AND @V1>@V2)
BEGIN
SET @RESULT=@V1-@V2
WHILE @RESULT>=0
BEGIN
	PRINT @RESULT
	SET @RESULT=@RESULT-@V2
END
END
ELSE
PRINT 'ENTER THE VALUES GREATER THAN 0 OR VAUE 1 MUST BE GREATER THAN VALUE 2'

--Q3

use test
CREATE TABLE INFO(
ID INT PRIMARY KEY,
UNAME VARCHAR(20) NOT NULL,
GENDER INT,
DOB DATE NOT NULL
)

insert into INFO VALUES 
(2, 'VINAY', '', '1997-04-16'), 
(3, 'VISHAL', 1, '1994-06-23'), 
(4, 'YASHIKA', 0, '1991-09-08'),
(5, 'ABCD', 2, '1996-12-18');

SELECT * FROM INFO;

UPDATE INFO
SET GENDER=NULL WHERE ID = 2

SELECT ID , UNAME, DOB,
CASE 
WHEN GENDER=0 THEN 'FEMALE'
WHEN GENDER=1 THEN 'MALE'
WHEN GENDER=2 THEN 'OTHER'
ELSE 'UNKNOWN' END
AS GENDER
FROM INFO

SELECT * 
Declare @dateofbirth datetime  
Declare @currentdatetime datetime  
Declare @years varchar(4)  
set @dateofbirth = DOB --Birthdate  
set @currentdatetime  = getdate() --Current Datetime  
SET @years = datediff(year,@dateofbirth,@currentdatetime)  
select @years   + ' years,' as years  
FROM INFO

DECLARE @date datetime, @tmpdate datetime, @years int, @months int, @days int
SELECT @date = '12/18/1996'

SELECT @tmpdate = @date

SELECT @years = DATEDIFF(yy, @tmpdate, GETDATE()) - CASE WHEN (MONTH(@date) > MONTH(GETDATE())) OR (MONTH(@date) = MONTH(GETDATE()) AND DAY(@date) > DAY(GETDATE())) THEN 1 ELSE 0 END
SELECT @tmpdate = DATEADD(yy, @years, @tmpdate)
SELECT @months = DATEDIFF(m, @tmpdate, GETDATE()) - CASE WHEN DAY(@date) > DAY(GETDATE()) THEN 1 ELSE 0 END
SELECT @tmpdate = DATEADD(m, @months, @tmpdate)
SELECT @days = DATEDIFF(d, @tmpdate, GETDATE())

SELECT @years AS YEARS, @months AS MONTHS, @days AS DAYS



USE test;
declare @currentdate date
set @currentdate= convert(date,getdate());

select *,DATEDIFF(mm,DOB,@currentdate)/12 as age,
concat(DATEDIFF(mm,DOB,@currentdate)/12,'YRS ',(DATEDIFF(mm,DOB,@currentdate)-((DATEDIFF(mm,DOB,@currentdate)/12)*12)),'MONTHS ',DATEDIFF(DD,day(DOB),day(@currentdate)),'DAYS') as EXACT_AGE  
from INFO
WHERE DATEDIFF(mm,DOB,@currentdate)/12  between 20 and 25;


CREATE OR ALTER PROCEDURE SP_SHOW
AS

declare @currentdate date
set @currentdate= convert(date,getdate());
select *,DATEDIFF(mm,DOB,@currentdate)/12 as age,
concat(DATEDIFF(mm,DOB,@currentdate)/12,'YRS ',(DATEDIFF(mm,DOB,@currentdate)-((DATEDIFF(mm,DOB,@currentdate)/12)*12)),'MONTHS ',DATEDIFF(DD,day(DOB),day(@currentdate)),'DAYS') as EXACT_AGE  
from INFO
WHERE DATEDIFF(mm,DOB,@currentdate)/12  between 20 and 25
GO

EXEC SP_SHOW;



use test
CREATE OR ALTER FUNCTION SALES(
    @quantity INT,
    @list_price DEC(10,2),
    @discount DEC(4,2)
)
RETURNS DEC(10,2)
AS 
BEGIN
    RETURN @quantity * @list_price * (1 - @discount);
END
GO

SELECT dbo.SALES(5,12,10)




use school
 
select * from Student_details;

SELECT *,
    RANK() OVER (PARTITION BY
                     student_Class
                 ORDER BY
                     Full_address
                ) rank
FROM
    Student_details;


	use test;
create or alter view new_info 
as
select * from INFO
where id>3;
go
select * from new_info;
select * from INFO;

WITH INFO_CTE( USERNAME,mob)
AS
(
SELECT UNAME, MobileNo
FROM INFO, table1
where INFO.ID=table1.id
)
SELECT USERNAME,mob
FROM INFO_CTE

select * from table1

USE test
SELECT
    ID, UNAME,GENDER,
  
    RANK() OVER (PARTITION BY
                     GENDER
                ORDER BY
                  UNAME DESC
                ) rank
	
FROM
    INFO;

	USE school
	SELECT * FROM Student_details;

	
	SELECT *,
    RANK() OVER (PARTITION BY
                    student_Class 
                ORDER BY
                  Full_address 
                ) rank
	from Student_details
