﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
namespace StringProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            int pos = 0;
            IList<int> myList = new List<int>();
            Console.WriteLine("List Program using (list<>)\n");
            myList.Add(1);
            myList.Add(2);
            myList.Add(5);
            myList.Add(7);
            myList.Add(7);
            myList.Add(9);
            myList.Add(3);
            myList.Add(0);
            myList.Add(2);
            myList.Add(3);
            myList.Add(4);
            myList.Add(6);
            myList.Add(8);
            foreach (int value in myList)
            {

                Console.WriteLine("Value of position "+pos+ " is => " +value);
                pos++;
            }
            Console.WriteLine("\nList Program using (list<string>)");
            List<string> strList = new List<string>();
            strList.Add("one");
            strList.Add("two");
            strList.Add("three");
            strList.Add("four");
            strList.Add("five");
            strList.Add(null);
            
            pos = 0;
            foreach (string value in strList)
            {

                Console.WriteLine("Value of position " + pos + " is => " + value);
                pos++;
            }
            Console.WriteLine("\nList Program using ( List<int>() { 10, 20, 30, 40 })");
            List<int> intList = new List<int>() { 10, 20, 30, 40 };
            pos = 0;
            Console.WriteLine("Index value of 20 = " + intList.IndexOf(20));
            foreach (int value in intList)
            {

                Console.WriteLine("Value of position " + pos + " is => " + value);
                pos++;
            }
            Console.WriteLine("\nList Program using (AddRange)");
            List<int> intList2 = new List<int>();

            intList2.AddRange(intList);

            pos = 0;
            foreach (int value in intList)
            {

                Console.WriteLine("Value of position " + pos + " is => " + value);
                pos++;
            }


            Console.WriteLine("\n\n****Observable Collection****");
            ObservableCollection<int> observableObj = new ObservableCollection<int>();
            Console.WriteLine("\nObject 1 values");
            observableObj.Add(2);
            observableObj.Add(4);
            observableObj.Add(6);
            observableObj.Add(9);
            foreach(int value in observableObj)
            {
                Console.WriteLine(value);
            }
            ObservableCollection<int> observableObjNew = observableObj;
            Console.WriteLine("\nObject 2 values");
            foreach (int value in observableObjNew)
            {
                Console.WriteLine(value);
            }
            observableObj.Add(5);
            observableObj.Add(6);
            observableObj.Remove(2);
            Console.WriteLine("\nObject 1 values after changes");
            foreach (int value in observableObj)
            {
                Console.WriteLine(value);
            }
            Console.WriteLine("\nObject 2 values after changes");
            foreach (int value in observableObjNew)
            {
                Console.WriteLine(value);
            }
            Console.ReadKey();
        }
    }
}