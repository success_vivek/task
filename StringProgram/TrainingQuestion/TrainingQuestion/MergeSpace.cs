﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace TrainingQuestion
{
    public class MergeSpace
    {
        public void SpaceMerge()
        {
            
            Console.Write("Enter any string : ");
            string strValue = Console.ReadLine();

            string resultStr = Regex.Replace(strValue, @"\s+", " ");
            Console.WriteLine(resultStr.Trim());

        }
       
    }
}
