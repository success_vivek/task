﻿using System;
using System.Text.RegularExpressions;
namespace TrainingQuestion
{
    class Program
    { 
        static void Main(string[] args)
        {
            MergeSpace Merge = new MergeSpace();
            StringSum StrSum = new StringSum();
            StringCountCharacters StrCount = new StringCountCharacters();
            Console.WriteLine("*********WELCOME TO THE PROGRAM*********\n");
            Console.WriteLine("1. Remove multiple spaces with a single space\n2. Sum of the digits in the string\n3. Count the length of the string");
            int input = Convert.ToInt16(Console.ReadLine());

            switch (input)
            {
                case 1:
                    Merge.SpaceMerge();
                    break;
                case 2:
                    StrSum.StringNoSum();
                    break;
                case 3:
                    StrCount.StringCount();
                    break;
                default:
                    break;
            }
            Console.ReadKey();
        }
    }
}