﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrainingQuestion
{
    class StringSum
    {
        public void StringNoSum()
        {
            int val;
            int sum = 0;
            Console.Write("Enter any string : ");
            string strValue = Console.ReadLine();
            for (int i = 0; i < strValue.Length; i++)
            {
                if (int.TryParse(strValue[i].ToString(), out val))
                {
                    Console.WriteLine("Value found in the String = " + val);
                    sum = sum + val;

                }

            }
             
                    Console.WriteLine("\nSum of the values         = "+sum);
        }
        
    }
}
