﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace TaskVehicle
{
    public class ExtraFunction
    {
        public void ArrList()
        {
            ArrayList arList = new ArrayList();
            arList.Add(300);
            arList.Add(200);
            arList.Add(100);
            arList.Add(500);
            arList.Add(400);

            Console.WriteLine("Original Order:");

            foreach (var item in arList)
                Console.WriteLine(item);

            arList.Reverse();

            Console.WriteLine("Reverse Order:");

            foreach (var item in arList)
                Console.WriteLine(item);

            arList.Sort();

            Console.WriteLine("Ascending Order:");

            foreach (var item in arList)
                Console.WriteLine(item);
            arList.Reverse();

            Console.WriteLine("Decending Order:");

            foreach (var item in arList)
                Console.WriteLine(item);
            Console.ReadKey();
        }
    }
}
