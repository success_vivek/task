﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskVehicle
{
   public class Month
    {
        public enum Months
        {
            January,
            Febrary,
            March,
            April,
            May,
            June,
            July,
            August,
            September,
            October,
            November,
            December
        }
        public void AddMonths()
        {
            Dictionary<Months, int> MonthDictionary = new Dictionary<Months, int>();
            MonthDictionary.Add(Months.January, 31);
            MonthDictionary.Add(Months.Febrary, 28);
            MonthDictionary.Add(Months.March, 31);
            MonthDictionary.Add(Months.April, 30);
            MonthDictionary.Add(Months.May, 31);
            MonthDictionary.Add(Months.June, 30);
            MonthDictionary.Add(Months.July, 31);
            MonthDictionary.Add(Months.August, 31);
            MonthDictionary.Add(Months.September, 30);
            MonthDictionary.Add(Months.October, 31);
            MonthDictionary.Add(Months.November, 30);
            MonthDictionary.Add(Months.December, 31);
            Console.Write("Enter the month name : ");
            string monthName=Console.ReadLine();
            DisplayMonths(MonthDictionary,monthName);
        }
        public void DisplayMonths(Dictionary<Months, int> MonDic, string str)
        {

            foreach(KeyValuePair<Months,int> key in MonDic)
            {
                if (key.Key.ToString() == str)
                {
                    Console.WriteLine(str + " contains " + key.Value + " Days");
                }
            }
        }
    }
}
