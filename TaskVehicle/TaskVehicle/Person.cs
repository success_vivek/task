﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskVehicle
{
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public void ListInput()
        {
            List<Person> ListObj = new List<Person>();
            ListObj.Add(new Person { Name = "Vivek", Age = 23 });
            ListObj.Add(new Person { Name = "Vicky", Age = 63 });
            ListObj.Add(new Person { Name = "Vishal", Age = 88 });
            ListObj.Add(new Person { Name = "Vinay", Age = 57 });
            ListObj.Add(new Person { Name = "Anand", Age = 75 });
            DisplayList(ListObj);

        }
        public void DisplayList(List<Person> obj)
        {
            for(int i=0;i<5;i++)
            {
                if (obj[i].Age>60)
                {
                    Console.WriteLine("Name : "+ obj[i].Name);
                    Console.WriteLine("Age : " + obj[i].Age);
                }

            } 
        }

        
    }
}
