﻿using System;
using System.Collections;

namespace TaskVehicle
{
    interface IPaint
    {
        void Paint();
    }
    interface SeatCover
    {
        void ChangeSheetCover();
    }
    public abstract class Vehicle : IPaint
    {
        private string vehicleName;
        private string color;
        public int wheels;
        public const int MaxSpeed = 60;
        public void Paint()
        {
            Console.WriteLine("This is paint method");
        }
        /// <summary>
        /// here is the property of the vehicle name
        /// </summary>
        public string VehicleName
        {
            get { return vehicleName; }
            set { vehicleName = value; }
        }
        public Vehicle(string vehName)
        {

        }
        /// <summary>
        /// here is the property of color of the vehicle
        /// </summary>
        public string Color
        {
            get { return color; }
            set { color = value; }
        }
        public int Wheels
        {
            get; set;

        }

        enum NoOfWheels
        {
            bike = 2,
            car = 4
        }
        public int VehicleWheel(string vName)                 // constructor of vehicle class
        {
            VehicleName = vName;
            if (VehicleName == "bike")
            {
                return (int)NoOfWheels.bike;
            }
            else
            {
                return (int)NoOfWheels.car;
            }
        }
        public void Start()                                // start method for start the vehicle
        {
            Console.WriteLine("Engine is Start");
        }
        public void Stop()                                 //stop method to stop the vehicle
        {
            Console.WriteLine("Engine is Stop");
        }
        public void SpeedUp(int speed)                     //speed up method to cheack the speed
        {
            Console.WriteLine(speed);
            if (speed > MaxSpeed)
            {
                Console.WriteLine("Alert! over speed");
                Stop();
            }
            else
            {
                Console.WriteLine("Vehicle is under speed");
                Start();
            }
        }
        public void DisplayDetails()                   // method to display all the details
        {
            Console.WriteLine("****** Vehicle information ******");
            Start();
            Console.WriteLine("vehicle name= " + VehicleName);
            Console.WriteLine("colour= " + Color);
            Console.WriteLine("wheels= " + Wheels);
            Console.WriteLine("max speed= " + MaxSpeed);
            Paint();
        }
        public abstract void CalculateTollAmount();
    }

    public class Car : Vehicle, SeatCover                          // defining car class inheriting the properties of vehicle class
    {
        public readonly int maxSpeed = 80;
        public int tollAmount;
        private static string veh;
        
        public void ChangeSheetCover()
        {
            Console.WriteLine("Car Sheet Cover Changed");
        }
        public int TollAmount
        {
            get { return tollAmount; }
            set { tollAmount = value; }
        }


        public override void CalculateTollAmount()
        {
            TollAmount = 50;
        }
        public Car(string newVehicleName, string newColour, int newMaxSpeed) : base(veh)         //constructor for car class
        {
            base.VehicleWheel(veh);
            veh = newVehicleName;
            base.VehicleName = veh;
            base.Color = newColour;                                      //initialize data members
            base.Wheels = base.VehicleWheel(veh);
            maxSpeed = newMaxSpeed;
        }
        public void Display()                                       // defining the display method to dispaly the data
        {
            CalculateTollAmount();
            Console.WriteLine("****** Updated Information ******");
            Console.WriteLine("vehicle name= " + veh);
            Console.WriteLine("toll amount= " + TollAmount);
            Console.WriteLine("colour= " + base.Color);
            Console.WriteLine("wheel= " + base.Wheels);
            Console.WriteLine("car speed= " + maxSpeed);
            ChangeSheetCover();
            SpeedUp(maxSpeed);

        }
    }
    /// <summary>
    /// instance of the bike class inheriting the vehicle class
    /// </summary>
    public class Bike : Vehicle, SeatCover           // defining bike class inheriting the properties of vehicle class
    {
        public readonly int maxSpeed = 90;
        public int tollAmount;
        private static string veh;

        public int TollAmount
        {
            get { return tollAmount; }
            set { tollAmount = value; }
        }

        
        public void ChangeSheetCover()
        {
            Console.WriteLine("Bike Sheet Cover Changed");
        }
        public override void CalculateTollAmount()
        {
            TollAmount = 30;
        }
        public Bike(string newVehicleName, string newColour, int newMaxSpeed) : base(veh)   //constructor for bike class
        {
            veh = newVehicleName;
            base.VehicleName = veh;
            base.Color = newColour;
            base.Wheels = base.VehicleWheel(veh);                          //initialize data members
            maxSpeed = newMaxSpeed;

        }
        public void Display()                           // defining the display method to dispaly the data
        {
            CalculateTollAmount();
            Console.WriteLine("****** Updated Information ******");
            Console.WriteLine("vehicle name= " + veh);
            Console.WriteLine("toll amount= " + TollAmount);
            Console.WriteLine("colour= " + base.Color);
            Console.WriteLine("wheel= " + base.Wheels);
            Console.WriteLine("bike speed= " + maxSpeed);
            ChangeSheetCover();
            SpeedUp(maxSpeed);
        }
    }
    /// <summary>
    /// main class of the program begins
    /// </summary>
    class Program
    {
        static void Main(string[] args)         //main method, here the program starts
        {
           
            string condition;
            do
            {
                Console.WriteLine("****** Car Engine Start / Stop ******");
                Console.Write("\nEnter the type of the vehicle (bike/car) : ");
                string vehicleNam = Console.ReadLine();
                if (vehicleNam == "car" || vehicleNam == "bike")
                {
                    Console.Write("Enter the color of the vehicle : ");
                    string vehicleColor = Console.ReadLine();
                    if (vehicleNam == "car")
                    {
                        Console.WriteLine();
                        Car objCar = new Car(vehicleNam, vehicleColor, 60);
                        objCar.DisplayDetails();
                        Console.Write("\nEnter Current speed : ");
                        int newSpeed = Convert.ToInt32(Console.ReadLine());
                        Car objCar2 = new Car(vehicleNam, vehicleColor, newSpeed);
                        objCar2.Display();
                    }

                    else
                    {
                        Console.WriteLine();
                        Bike objBike = new Bike(vehicleNam, vehicleColor, 60);
                        objBike.DisplayDetails();
                        Console.Write("\nEnter Current speed : ");
                        int newSpeed = Convert.ToInt32(Console.ReadLine());
                        Bike objBike2 = new Bike(vehicleNam, vehicleColor, newSpeed);
                        objBike2.Display();
                    }
                }
                else
                {
                    Console.WriteLine("Select bike or car!  try again");
                }
                Console.WriteLine("do you want to continue Y/N");
                condition = Console.ReadLine();
            } while (condition == "y" || condition == "Y");
            //object for the month Class
            Console.WriteLine("Months program");
            Month mon = new Month();
            mon.AddMonths();
            //object for the Person Class
            Console.WriteLine("\nPerson program");
            Person PersonObj = new Person();
            PersonObj.ListInput();

           // ArrayList arList = new ArrayList();
            Console.ReadKey();
        }
    }
}