﻿using System;

namespace TrainingSession2
{
   public class Vehicle
    {
        private string vehicleName;
        public string color = "red";
        public int wheels;
        public const int maxSpeed = 60;
      public  Vehicle(string vehicleName)                 // constructor of vehicle class
        {
            this.vehicleName = vehicleName;
            if (this.vehicleName == "bike")
            {
                wheels = 2;
            }
            else
            {
                wheels = 4;
            }
        }
       public void Start()                                // start method for start the vehicle
        {
            Console.WriteLine("Start");
        }
       public void Stop()                                 //stop method to stop the vehicle
        {
            Console.WriteLine("stop");
        }
        public void SpeedUp(int speed)                     //speed up method to cheack the speed
        {
            Console.WriteLine(speed);
            if (speed > maxSpeed)
            {
                Console.WriteLine("over speed");
            }
            else
            {
                Console.WriteLine("under speed");
            }
        }
        public void DisplayDetails()                   // method to display all the details
        {
            Console.WriteLine("vehicle name= " + vehicleName);
            Console.WriteLine("colour= " + color);
            Console.WriteLine("wheels= " + wheels);
            Console.WriteLine("max speed= " + maxSpeed);
        }
    }
    public class NewVehicle
    {
        static void Main(string[] args)         //main method, here the program starts
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine("enter the name of the vehicle");
            string vehicleNam = Console.ReadLine();
            Vehicle myVehicle = new Vehicle(vehicleNam);        // making the object of the vehicle class
            myVehicle.Start();                                  //call the start method
            Console.WriteLine("enter the speed");
            int speedInput = Convert.ToInt32(Console.ReadLine());
            myVehicle.SpeedUp(speedInput);                      // call speedup method
            myVehicle.Stop();                                   // call stop method
            Console.WriteLine();
            myVehicle.DisplayDetails();                         // call method to display details
            Console.ReadKey();                                     // hold output screen

        }
    }
}
