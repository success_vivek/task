
function validateForm() {
  var name = document.forms["submitForm"]["name"].value;
  if (name == ""||name.length>100) {
	document.getElementById('errorName').innerHTML="Name can't be empty or length is not greater than 100";
	document.getElementById('errorName').style.color="red";
  }
  
  var dob = document.forms["submitForm"]["DOB"].value;
  if (dob == "") {
    document.getElementById('errorDob').innerHTML="Date of birth can't be empty";   
	document.getElementById('errorDob').style.color="red";
  }

  var phone = document.forms["submitForm"]["phone"].value;
  if (phone == ""||phone.length!=10) {
    document.getElementById('errorPhone').innerHTML="Phone number can't be empty or length not equal to 10";
	document.getElementById('errorPhone').style.color="red";
  }
  
  var email = document.forms["submitForm"]["email"].value;
  if (email == ""||email.length>256) {
    document.getElementById('errorEmail').innerHTML="Email can't be empty or length not more than 256";
	document.getElementById('errorEmail').style.color="red";
  }
  
  var department = document.forms["submitForm"]["dept"].value;
  if (department == "Select department") {
    document.getElementById('errorDept').innerHTML="Select a Department";
	document.getElementById('errorDept').style.color="red";
  }
  
  var address = document.forms["submitForm"]["address"].value;
  if (address == "") {
    document.getElementById('errorAddress').innerHTML="Address can't be empty";
	document.getElementById('errorAddress').style.color="red";
  }
  
  var state = document.forms["submitForm"]["state"].value;
  if (state == "Choose State") {
    document.getElementById('errorState').innerHTML="State can't be empty";
	document.getElementById('errorState').style.color="red";
  }
  
  var city = document.forms["submitForm"]["city"].value;
  if (city == "Choose city") {
    document.getElementById('errorCity').innerHTML="City can't be empty";  
	document.getElementById('errorCity').style.color="red";
  }
  
  var zip = document.forms["submitForm"]["zip"].value;
  if (zip == ""||zip.length!=6) {
    document.getElementById('errorZip').innerHTML="Zip can't be empty and must have 6 digits";
	document.getElementById('errorZip').style.color="red";
 
  }
  if(name == ""||name.length>100||dob == ""||phone == ""||phone.length!=10||email == ""||email.length>256||department == "Select department"||address == ""||state == "Choose State"||city == "Choose city"||zip == ""||zip.length!=6)
  {
	  return false;
  }
}


// function DateFormat(){
 // var today = new Date(); 
 // var dd = today.getDate(); 
 // var mm = today.getMonth()+1; 
 // //January is 0! 
 // var yyyy = today.getFullYear(); 
 // if(dd<10){dd='0'+dd} 
 // if(mm<10){mm='0'+mm} 
 // var today = dd+'/'+mm+'/'+yyyy; 
 // document.getElementById("join").value = today;
 // document.getElementById("join").innerHTML=today;
// }


var citiesByState = {
Chennai: ["Choose city","Chennai"],
Maharashtra: ["Choose city","Mumbai","Pune","Bhopal"],
Delhi: ["Choose city","New Delhi","Old Delhi"]
}
function makeSubmenu(value) {
	
if(value.length==0) document.getElementById("city").innerHTML = "<option></option>";
else {
var citiesOptions = "";
for(cityId in citiesByState[value]) {
citiesOptions+="<option>"+citiesByState[value][cityId]+"</option>";
}
document.getElementById("city").innerHTML = citiesOptions;
}
}

function changeEmptyField(value,id){
	if(value==""){
	document.getElementById(id).innerHTML="Field is empty";
	}
	else{
		document.getElementById(id).innerHTML="";
	}
}

