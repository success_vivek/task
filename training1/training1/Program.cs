﻿using System;

namespace Training1
{
    class Program
    {
        public void DataConversion()
        {

            char readInput;
            Console.Write("Enter a character: ");
            readInput = Console.ReadLine()[0];
            int integerData = readInput;
            long longValue = readInput;
            long longValue_2 = integerData;
            char characterData = readInput;
            float floatData = longValue;
            float floatData_2 = characterData;
            double doubleData = floatData_2;

            Console.WriteLine("Type conversion");
            Console.WriteLine("Implicit type conversions");
            Console.WriteLine("Value of char is " + readInput);
            Console.WriteLine("value of int is" + integerData);
            Console.WriteLine("value of long is" + longValue);
            Console.WriteLine("value of float is" + floatData);
            Console.WriteLine("value of double is" + doubleData);
            Console.WriteLine("value of long is" + longValue_2);
            Console.WriteLine("value of float is" + floatData_2);


            Console.WriteLine("\nexplicit type conversions");

            
            Console.Write("Enter a number in double data type: ");
            double doubleInput=Convert.ToDouble(Console.ReadLine());
            float floatInput = (float)doubleInput;
            long longInput = (long)doubleInput;
            int integerInput = (int)doubleInput;
            Console.WriteLine("double=" + doubleInput);
            Console.WriteLine("float=" + floatInput);
            Console.WriteLine("long=" + longInput);
            Console.WriteLine("int=" + integerInput);
            Console.WriteLine("\nother conversions");
            Console.WriteLine("Flaot to string=" + Convert.ToString(floatInput));
            Console.WriteLine("int to double= " + Convert.ToDouble(integerInput));
            Console.WriteLine("double to int 32=" + Convert.ToInt32(doubleInput));
            Console.WriteLine("float to uint32=" + Convert.ToUInt32(floatInput));
        }

        void StringReference()
        {
            string stringInput = "hello";
            string stringInput_2 = stringInput;
            Console.WriteLine("string s=" + stringInput);
            Console.WriteLine("string r=" + stringInput_2);
            stringInput = "world";
            Console.WriteLine("modified string s=" + stringInput);
            Console.WriteLine("modified string r=" + stringInput_2);
        }

        void Colour(ref string colorName)
        {

            switch (colorName)
            {
                case "red":
                case "green":
                case "blue":
                    Console.WriteLine("colour is red or blue or green");
                    break;
                default:
                    Console.WriteLine("colour is not present");
                    break;
            }

            if (colorName == "red" || colorName == "green" || colorName == "blue")
            {
                Console.WriteLine("colour is present");
            }
            else
            {
                Console.WriteLine("colour is not present");
            }


            var colorResult = colorName == "red" || colorName == "green" || colorName == "blue" ? "colour is present" : "colour is not present";

            Console.WriteLine(colorResult);
        }

        void Months()
        {
            int monthIndex;
            string[] monthsName = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            for (monthIndex = 0; monthIndex < monthsName.Length; monthIndex++)
            {
                if (monthsName[monthIndex][0] == 'j' || monthsName[monthIndex][0] == 'J')
                {
                    Console.WriteLine(monthsName[monthIndex]);
                }
            }
        }
        public void CheckNullable(int? inputValue)
        {
            if (inputValue == null)
                Console.WriteLine("true");
            else
                Console.WriteLine("False");
        }

        static void Main(string[] args)
        {
            Program programObject = new Program();
            Console.WriteLine("1) Possible data conversion, implicit and explicit conversions.");
            Console.WriteLine("\n2) String data type work as a reference type");
            Console.WriteLine("\n3) Months starting with J. You can use FOR/FOREACH/WHILE loop.");
            Console.WriteLine("\n4) Find out if the color passed is either Red, Green or Blue.");
            Console.WriteLine("\n5) A method that will take an integer as a nullable parameter");
            Console.WriteLine("\n6) exit");
            Console.WriteLine("Tasks from 1 to 6");
            int userInput = Convert.ToInt32(Console.ReadLine());
            switch (userInput)
            {
                case 1:
                    programObject.DataConversion();
                    break;
                case 2:
                    programObject.StringReference();
                    break;
                case 3:
                    programObject.Months();
                    break;
                case 4:
                    string s = Console.ReadLine();

                    programObject.Colour(ref s);
                    break;
                case 5:
                    programObject.CheckNullable(2);
                    programObject.CheckNullable(null);
                    break;
                default:
            
                    break;
            }
            Console.ReadKey();
        }
    }
}